jest.dontMock('../validator');

describe('validator', function () {
  var validator = require('../validator');

  // validators.isbn()
  describe('validators.isbn()', function () {
    it('must return `false`, when the given ISBN is not in ISBN-10 format', function () {
      expect(validator.validators.isbn(123)).toEqual(false);
      expect(validator.validators.isbn()).toEqual(false);
      expect(validator.validators.isbn('blah')).toEqual(false);
    });
    it('must be truthy, when the given ISBN is in ISBN-10 format', function () {
      expect(validator.validators.isbn('123456789X')).toEqual(true);
      expect(validator.validators.isbn('1234567897')).toEqual(true);
    });
  });

  // validators.title()
  describe('validators.title()', function () {
    it('must be falsy, when the given Title is empty string', function () {
      expect(validator.validators.title('')).toEqual(false);
    });
    it('must be falsy, when the given Title has more then 50 chars', function () {
      expect(validator.validators.title('123456789012345678901234567890123456789012345678901')).toEqual(false);
    });
    it('must be truthy, when the given Title is NOT an empty string and less then 50 chars', function () {
      expect(validator.validators.title('The best book ever 1234567890123456789012345678901')).toEqual(true);
    });
  });

  // validators.year()
  describe('validators.year()', function () {
    it('must be falsy, if the given year is NOT in the "1945 to currentYear()" range', function () {
      expect(validator.validators.year('1944')).toEqual(false);
      expect(validator.validators.year('2017')).toEqual(false);
    });
    it('must be truthy, if the given year is in the "1945 to currentYear()" range', function () {
      expect(validator.validators.year('1945')).toEqual(true);
      expect(validator.validators.year('2000')).toEqual(true);
      expect(validator.validators.year('2016')).toEqual(true);
    });
  });

  // validate
  describe('validate()', function () {
    beforeEach(function () {
      validator.errors = {};
    });

    it('must populate `errors.isbn` with the error message if `isbn` is not valid', function () {
      validator.validate({
        isbn: '123'
      });
      expect(validator.errors.isbn).toEqual('Please supply a valid ISBN in ISBN-10 format!');
    });

    it('must populate `errors.title` with the error message if `title` is not valid', function () {
      validator.validate({
        title: ''
      });
      expect(validator.errors.title).toEqual('Title can\'t be an empty string and can contain maximum of 50 characters!');
    });

    it('must populate `errors.year` with the error message if `year` is not valid', function () {
      validator.validate({
        year: 1940
      });
      expect(validator.errors.year).toEqual('Please supply a valid year in the range from 1459 to the current one!');
    });

    it('must populate `errors` with the error message for each field if it is not valid', function () {
      validator.validate({
        isbn: '123',
        title: '',
        year: 1940
      });

      expect(validator.errors.isbn).toEqual('Please supply a valid ISBN in ISBN-10 format!');
      expect(validator.errors.title).toEqual('Title can\'t be an empty string and can contain maximum of 50 characters!');
      expect(validator.errors.year).toEqual('Please supply a valid year in the range from 1459 to the current one!');
    });

    it('must NOT populate `errors` with the error message for each field if it is valid', function () {
      validator.validate({
        isbn: '123456789X',
        title: 'Book',
        year: 1946
      });

      expect(validator.errors.isbn).toBeFalsy();
      expect(validator.errors.title).toBeFalsy();
      expect(validator.errors.year).toBeFalsy();
    });
  });

  describe('isValid()', function () {
    it('must return `false` if the some data chunk is NOT valid', function () {
      validator.validate({
        isbn: '123', // isbn is NOT invalid
        title: 'Book',
        year: 1946
      });

      expect(validator.isValid()).toBe(false);
    });

    it('must return `true` if the provided data is valid', function () {
      validator.validate({
        isbn: '123456789X', // isbn is invalid
        title: 'Book',
        year: 1946
      });

      expect(validator.isValid()).toBe(true);
    });
  });
});