module.exports = {
  errors: {},
  errorMessages: {
    isbn: 'Please supply a valid ISBN in ISBN-10 format!',
    title: 'Title can\'t be an empty string and can contain maximum of 50 characters!',
    year: 'Please supply a valid year in the range from 1459 to the current one!',
    name: 'Name can\'t be an empty string and can contain maximum of 50 characters!',
    email: 'Please supply a valid email!',
    authorId: 'ID can\'t be an empty string and can contain maximum of 50 characters!',
  },
  validators: {
    isbn: function (value) {
      return /^\d{9}(\d|X)$/.test(value);
    },

    title: function (value) {
      return /^.{1,50}$/.test(value);
    },

    year: function (value) {
      var year = new Date().getFullYear().toString(),
          pattern = '^(194[5-9]|19[5-9]\\d|200\\d|'
                      + year.substr(0,3)
                      + '[0-'
                      + year.substr(3) +'])$';

      return (new RegExp(pattern)).test(value);
    },

    name: function (value) {
      return /^.{1,50}$/.test(value);
    },

    email: function (value) {
      return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
    },

    authorId: function (value) {
      return /^.{1,50}$/.test(value);
    },
  },

  validate: function (data) {
    this.errors = {};

    for (var key in data) {
      if (!this.validators.hasOwnProperty(key)) {
        return console.warn('[validator.js]: There is no such vaidator!');
      }

      if (!this.validators[key](data[key])) {
        this.errors[key] = this.errorMessages[key];
      }
    }
  },

  isValid: function () {
    return Object.keys(this.errors).every((function (error) {
      return !this.errors[error];
    }).bind(this));
  }
};